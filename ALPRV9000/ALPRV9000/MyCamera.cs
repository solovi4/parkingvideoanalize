﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ALPRV9000
{
    class MyCamera
    {
        Mat _frame = new Mat();
        Mat _frameCopy = new Mat();
        private Capture _capture = null;
        private bool _captureInProgress;

        public MyCamera()
        {
            _capture = new Capture();
            _capture.ImageGrabbed += ProcessFrame;
        }

        public MyCamera(string ip, int port, string login, string password)
        {
            string connection_string = "rtsp://" + login + ":" + password + "@" + ip + "/StreamingSetting?version=1.0&action=getRTSPStream&ChannelID=1&ChannelName=Channel1";            
            _capture = new Capture(connection_string);//Сетевая камера  
            _capture.ImageGrabbed += ProcessFrame;
        }
        public delegate void GetFrame(Bitmap bitMap);
        GetFrame getFrame;
        public void setCallBack(GetFrame getFrame)
        {
            this.getFrame = getFrame;
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero)
            {
                _capture.Retrieve(_frame, 0);
                getFrame(_frame.Bitmap);              
            }
        }

        public void Start()
        {
            _captureInProgress = true;
            _capture.Start();
        }

        public void Stop()
        {
            _captureInProgress = false;
            _capture.Stop();
        }

        public bool getCaptureProgress()
        {
            return _captureInProgress;
        }

        public void Pause()
        {
            _captureInProgress = false;
            _capture.Pause();
        }
    }
}
