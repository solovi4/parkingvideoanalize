﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using openalprnet;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Diagnostics;
using Emgu.CV.UI;
using Emgu.CV.Util;
using System.Threading;
using System.Text.RegularExpressions;

namespace ALPRV9000
{
    public partial class Form1 : Form
    {
        NumberRecognizer numberRecognizer;
        public Form1()
        {
            InitializeComponent();
            FormSettings.Load();
            numberRecognizer = new NumberRecognizer();
            bgw = new BackgroundWorker();
            bgw.DoWork += Bgw_DoWork;
            bgw.RunWorkerCompleted += Bgw_RunWorkerCompleted;
            lastNumbers = new List<string>();
            numbers = new List<string>();

        }

        private void Bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            printNumber();
        }

        private void Bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            Bitmap bm = (Bitmap)e.Argument;
            findNumber(bm);
        }

        BackgroundWorker bgw;
        private void button1_Click(object sender, EventArgs e)
        {
        
            OpenFileDialog of = new OpenFileDialog();
            if(of.ShowDialog() == DialogResult.OK)
            {
                Bitmap bitMap = new Bitmap(of.FileName);
                if (!bgw.IsBusy)
                {
                    picOriginal.Image = bitMap;
                    bgw.RunWorkerAsync(bitMap);
                }
                    
            }       
        }

        List<string> lastNumbers;
        private void findNumber(Bitmap imageSource)
        {
            lastNumbers = numberRecognizer.getNumberCar(imageSource);            
        }

        string getMostPopular(List<string> arr)
        {
            string popular = "";
            int[] entries = new int[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                for(int j=0; j<arr.Count; j++)
                {
                    if (i == j) continue;
                    if (arr[i] == arr[j])
                        entries[i]++;
                }
            }

            int index = -1;
            int max = -1;
            if (entries.Length>0)
                max = entries.Max();
            for (int i=0;i<entries.Length; i++)
            {
                if (entries[i] == max)
                {
                    index = i;
                    break;
                }
                    
            }            
           
            if(index>-1)
                popular = arr[index];
            return popular;
        }

        DateTime time;
        TimeSpan pause = new TimeSpan(0, 0, 5);
        List<string> numbers;
        void printNumber()
        {
            TimeSpan raznost = DateTime.Now - time;
            if(raznost>pause)
            {
                numbers.Clear();
                lbxPlates.Items.Clear();
            }
            if(lastNumbers.Count>0)
                time = DateTime.Now;
            numbers.AddRange(lastNumbers.ToArray());
            lbxPlates.Items.AddRange(lastNumbers.ToArray());
            label1.Text = "Вероятно номер " + getMostPopular(numbers);
               
            panel1.Controls.Clear();
            int offset_x = 0;
            List<Bitmap> steps = numberRecognizer.getStepsImages();
            foreach (var step in steps)
            {
                PictureBox picBox = new PictureBox();
                picBox.Size = new Size(step.Width, step.Height);
                picBox.Location = new Point(offset_x, 0);
                picBox.Image = step;
                panel1.Controls.Add(picBox);
                offset_x += step.Width + 10;
            }

            /*
            int offset_y = 0;
            panel2.Controls.Clear();
            foreach (var cr in numberRecognizer.compare_results)
            {
                PictureBox picBox = new PictureBox();
                picBox.Size = new Size(cr.img1.Width, cr.img1.Height);
                picBox.Location = new Point(0, offset_y);
                picBox.Image = cr.img1;
                panel2.Controls.Add(picBox);

                PictureBox picBox2 = new PictureBox();
                picBox2.Size = new Size(cr.img2.Width, cr.img2.Height);
                picBox2.Location = new Point(cr.img1.Width, offset_y);
                picBox2.Image = cr.img2;
                panel2.Controls.Add(picBox2);
                offset_y += Math.Max(picBox.Height, picBox2.Height);
                panel2.Controls.Add(picBox2);

                Label lab = new Label();
                lab.Text = cr.result;
                lab.Location = new Point(picBox2.Width + picBox2.Location.X, picBox2.Location.Y);
                panel2.Controls.Add(lab);
                offset_y += 5;

                RichTextBox rtb = new RichTextBox();
                rtb.Text = cr.details;
                rtb.Location = new Point(0, offset_y);
                rtb.Size = new Size(panel2.Width, 40);
                offset_y += rtb.Height+20;
                panel2.Controls.Add(rtb);
            }
            */
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormSettings fs = new FormSettings();
            fs.ShowDialog();
        }

        delegate void GetFrame(Bitmap bitMap);
        public void getFrame(Bitmap bitMap)
        {
            GetFrame del = getFrame;
            if (InvokeRequired)
            {
                try
                {
                    this.Invoke(del, bitMap);
                }
                catch
                {}
                return;
                
            }
            picOriginal.Image = bitMap;
            if(!bgw.IsBusy)
                bgw.RunWorkerAsync(bitMap);
            //findNumber(bitMap);
        }
     

        MyCamera myCam;
        private void startCamerabutton_Click(object sender, EventArgs e)
        {
            startCameraButton.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;
            if (FormSettings.ThisIsNetCamera)
                myCam = new MyCamera(FormSettings.IP_Address, FormSettings.Port, FormSettings.Login, FormSettings.Password);
            else
                myCam = new MyCamera();

            MyCamera.GetFrame gf = getFrame;
            myCam.setCallBack(gf);
            myCam.Start();
            stopCameraButton.Enabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void stopCameraButton_Click(object sender, EventArgs e)
        {
            stopCameraButton.Enabled = false;
            startCameraButton.Enabled = true;
            myCam.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {    
            if(myCam!=null)        
                myCam.Stop();
        }
    }
}
