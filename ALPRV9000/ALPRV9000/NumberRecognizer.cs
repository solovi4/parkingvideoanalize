﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using openalprnet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Tesseract;
namespace ALPRV9000
{
    class NumberRecognizer
    {
        private TesseractEngine _ocr;
        public NumberRecognizer()
        {
            //create OCR engine         
            _ocr = new TesseractEngine("NumberRecognizer", "rus", EngineMode.TesseractOnly);
            _ocr.SetVariable("tessedit_char_whitelist", "АВЕКМНОРСТУХ-1234567890");
            numberPlate = new List<Bitmap>();
            images_steps = new List<Bitmap>();
            numbers = new List<string>();
            compare_results = new List<CompareResult>();
        }
        private List<Bitmap> images_steps;
        public List<CompareResult> compare_results;    
        public List<Bitmap> getStepsImages()
        {
            return images_steps;
        }    
        
        private string getNumberFromContours(List<Bitmap> segments)
        {
            string numbers = "1234567890";
            string letters = "АВЕКМНОРСТУХ";
            string letters_numbers = letters+"-"+numbers;
            string[] whitelist = {letters, numbers, numbers, numbers, letters, letters};
            string number = "";//Тут будет номер
         
            for (int i=0; i<segments.Count; i++)
            {
                /*
                if(i<6)
                {
                    _ocr.SetVariable("tessedit_char_whitelist", whitelist[i]);
                }
                else
                {
                    _ocr.SetVariable("tessedit_char_whitelist", letters_numbers);
                }*/

                
                
                string text = "";
                using (Page page = _ocr.Process(segments[i], PageSegMode.SingleChar))
                {
                    text = page.GetText();
                    number += text;
                }
                
            }

            return number;
        }

        private List<Bitmap> bitMapFromUmat(List<UMat> umats)
        {
            List<Bitmap> bitmaps = new List<Bitmap>();
            foreach (var umat in umats)
            {
                bitmaps.Add((Bitmap)umat.Bitmap.Clone());
            }
            return bitmaps;
        }

        private string getNumberFromPlate(Mat plate)
        {           
           
            string number = "";//Тут будет номер            
            _ocr.SetVariable("tessedit_char_whitelist", "АВЕКМНОРСТУХ-1234567890");                          

            using (Bitmap tmp = plate.Bitmap)
            {
                string text = "";
                using (Page page = _ocr.Process(tmp, PageSegMode.SingleLine))
                {
                    text = page.GetText();
                    number += text;
                }
            }
            

            return number;
        }

        private void AddUmatToLog(List<UMat> images)
        {
            foreach (var image in images)
                images_steps.Add(image.Clone().Bitmap);
        }

        private void AddBitmapToLog(Bitmap image)
        {
            images_steps.Add(image);
        }

        private void AddBitmapsToLog(List<Bitmap> images)
        {
            images_steps.AddRange(images);
        }

        private Mat Resize(Mat source, Size size)
        {
            Mat dest = new Mat();            
            double scale = Math.Min((float)size.Width / (float)source.Size.Width, (float)size.Height / (float)source.Size.Height);
            Size newSize = new Size((int)Math.Round(source.Size.Width * scale), (int)Math.Round(source.Size.Height * scale));
            CvInvoke.Resize(source, dest, newSize, 0, 0, Inter.Cubic);
            return dest;                       
        }

        private UMat Resize(UMat source, Size size)
        {
            UMat dest = new UMat();
            double scale = Math.Min((float)size.Width / (float)source.Size.Width, (float)size.Height / (float)source.Size.Height);
            Size newSize = new Size((int)Math.Round(source.Size.Width * scale), (int)Math.Round(source.Size.Height * scale));
            CvInvoke.Resize(source, dest, newSize, 0, 0, Inter.Cubic);
            return dest;
        }

        private List<UMat> Resize(List<UMat> sources, Size size)
        {
            List<UMat> dest = new List<UMat>();
            foreach (var src in sources)
            {
                dest.Add(Resize(src, size));
            }
            return dest;
        }

        private Bitmap extendBitmap(Bitmap srcBitmap, Size newSize, Point point, Brush color)
        {
            Bitmap newImage = new Bitmap(newSize.Width, newSize.Height, srcBitmap.PixelFormat);
            using (Graphics g = Graphics.FromImage(newImage))
            {
                // fill target image with white color
                g.FillRectangle(color, 0, 0, newSize.Width, newSize.Height);
                // place source image inside the target image
                g.DrawImage(srcBitmap, point.X, point.Y);
            }           
            return newImage;
        }

        private List<Bitmap> extendBitmaps(List<Bitmap> srcBitmaps, int addWidth, int addHeight, Point point, Brush color)
        {
            List<Bitmap> extendedBitmaps = new List<Bitmap>();
            foreach (var bitmap in srcBitmaps)
            {
                extendedBitmaps.Add(extendBitmap(
                    new Bitmap(bitmap), new Size(bitmap.Width + addWidth, bitmap.Height + addHeight), new Point(addWidth / 2, addHeight / 2), Brushes.Black));
            }
            return extendedBitmaps;
        }

        private Mat BitMapToMat(Bitmap bgr_image)
        {  
            Image<Bgr, byte> img1 = new Image<Bgr, byte>(bgr_image);   
            return img1.Mat;
        }

        private List<RotatedRect> getBoxesFromContours(VectorOfVectorOfPoint contours, Size minSize, Size maxSize)
        {
            List<RotatedRect> boxes = new List<RotatedRect>();
            for (int i = 0; i < contours.Size; i++)
            {
                RotatedRect box = CvInvoke.MinAreaRect(contours[i]);
                if (box.Size.Height < minSize.Height || box.Size.Width < minSize.Width) continue;
                if (box.Size.Height > maxSize.Height || box.Size.Width > maxSize.Width) continue;
                boxes.Add(box);
            }

            return boxes;
        }


        
        

        private bool cmpBoxes(RotatedRect box1, RotatedRect box2)
        {
            bool result = false;//Если не равны

            //Левый нижний угол box1
            float box1_x1 = box1.Center.X - box1.Size.Width / 2;
            float box1_y1 = box1.Center.Y - box1.Size.Height / 2;

            //Правый верхний угол box1
            float box1_x2 = box1.Center.X + box1.Size.Width / 2;
            float box1_y2 = box1.Center.Y + box1.Size.Height / 2;

            //Левый нижний угол box2
            float box2_x1 = box2.Center.X - box2.Size.Width / 2;
            float box2_y1 = box2.Center.Y - box2.Size.Height / 2;

            //Правый верхний угол box2
            float box2_x2 = box2.Center.X + box2.Size.Width / 2;
            float box2_y2 = box2.Center.Y + box2.Size.Height / 2;


            float tolerance_x = box1.Size.Width / 7;
            float tolerance_y = box1.Size.Height / 7;
            if (box1_x1>box2_x1 && box1_x2 < box2_x2)
            {
                if(box1_y1 > box2_y1 && box1_y2 < box2_y2)
                {
                    result = true; //Box1 внутри Box2
                }
            }

            if (box2_x1 > box1_x1 && box2_x2 < box1_x2)
            {
                if (box2_y1 > box1_y1 && box2_y2 < box1_y2)
                {
                    result = true; //Box2 внутри Box1
                }
            }
            
            

            if (box1.Center.X < box2.Center.X + tolerance_x && box1.Center.X > box2.Center.X - tolerance_x)
            {
                if (box1.Center.Y < box2.Center.Y + tolerance_y && box1.Center.Y > box2.Center.Y - tolerance_y)
                {
                    if(box1.Size.Height< box2.Size.Height+ tolerance_x && box1.Size.Height > box2.Size.Height - tolerance_x)
                    {
                        if (box1.Size.Width < box2.Size.Width + tolerance_y && box1.Size.Width > box2.Size.Width - tolerance_y)
                        {
                            result = true;
                        }
                    }
                    
                }
            }
            /*
            string details =
                "box1 center(" + (int)box1.Center.X + " и " + (int)box1.Center.Y + ") size(" + (int)box1.Size.Width + " и " + (int)box1.Size.Height + ")" + System.Environment.NewLine+
                "box2 center(" + (int)box2.Center.X + " и " + (int)box2.Center.Y + ") size(" + (int)box2.Size.Width + " и " + (int)box2.Size.Height + ")";
            List<UMat> Umats = getNormImagesInContours(new List<RotatedRect>(new RotatedRect[] { box1, box2 }), filteredGray_global);
            CompareResult cr = new CompareResult(Umats[0].Bitmap, Umats[1].Bitmap, result.ToString(), details);
            compare_results.Add(cr);*/
                return result;
        }


        private List<int> checkBoxInArray(List<RotatedRect> boxes, RotatedRect box)
        {
            List<int> indexes_remove = new List<int>();
            for (int i=0; i< boxes.Count; i++)
            {
                if(cmpBoxes(boxes[i], box))
                {
                    
                     indexes_remove.Add(i);
                                        
                } 
                
            }
            return indexes_remove;
        }

        private List<RotatedRect> removeRepeatedBoxes(List<RotatedRect> boxes)
        {
            List<RotatedRect> boxes_result = new List<RotatedRect>();
            List<int> indexes_remove = new List<int>();
            for (int i=0; i<boxes.Count; i++)
            {
                indexes_remove = checkBoxInArray(boxes_result, boxes[i]);
                if (indexes_remove.Count() == 0)
                    boxes_result.Add(boxes[i]); //Если в массиве нет такого
                else
                {
                    List<RotatedRect> temp = new List<RotatedRect>();
                    for (int z=0;z<boxes_result.Count; z++)
                    {
                        if (indexes_remove.Contains(z)) continue;
                        temp.Add(boxes_result[z]);
                    }
                    boxes_result = new List<RotatedRect>(temp.ToArray<RotatedRect>());
                    boxes_result.Add(boxes[i]); //Если в массиве нет такого

                }
                    

            }
            return boxes_result;
        }

        private List<RotatedRect> sortBoxes(List<RotatedRect> boxes)
        {
            List<RotatedRect> sortBoxes = new List<RotatedRect>(boxes);
            for(int i = 0; i< sortBoxes.Count-2; i++)
            {
                bool exchange = false;
                for(int j=0; j<sortBoxes.Count-i-1; j++)
                {
              
                    if(sortBoxes[j].Center.X > sortBoxes[j+1].Center.X)
                    {
                        RotatedRect tmp = sortBoxes[j];
                        sortBoxes[j] = sortBoxes[j+1];
                        sortBoxes[j+1] = tmp;
                        exchange = true;
                    }
                }
                if (!exchange) break;
            }
            return sortBoxes;
        }
        private List<UMat> getNormImagesInContours(List<RotatedRect> boxes, Mat from)
        {
            List<UMat> imagesMat = new List<UMat>(); 
            foreach (var box in boxes)
            {
                float height = Math.Max(box.Size.Height, box.Size.Width);
                float width = Math.Min(box.Size.Height, box.Size.Width);
                //float height = box.Size.Height;
                //float width = box.Size.Width;
                PointF[] destCorners;
                if (box.Size.Height > box.Size.Width)
                {
                    destCorners = new PointF[] {
                        new PointF(0, height - 1),
                        new PointF(0, 0),
                        new PointF(width - 1, 0),
                        new PointF(width - 1, height - 1)};
                }
                else
                {
                    destCorners = new PointF[] {
                        new PointF(width - 1, height - 1),
                        new PointF(0, height - 1),
                        new PointF(0, 0),
                        new PointF(width - 1, 0)};
                }
                PointF[] srcCorners = box.GetVertices();
               
                
                using (Mat rot = CvInvoke.GetAffineTransform(srcCorners, destCorners))
                using (UMat tmp1 = new UMat())
                {
                    CvInvoke.WarpAffine(from, tmp1, rot, Size.Round(new SizeF(width, height)));
                    imagesMat.Add(tmp1.Clone());
                }
            }
            return imagesMat;
        }

        private UMat FilterPlate(UMat plate)
        {
            UMat thresh = new UMat();
            CvInvoke.Threshold(plate, thresh, 100, 255, ThresholdType.BinaryInv);
            CvInvoke.Erode(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
            CvInvoke.Dilate(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);

            return thresh;
        }

        private Mat FilterPlateMat(Mat plate)
        {
            Mat thresh = new Mat();
            CvInvoke.Threshold(plate, thresh, 100, 255, ThresholdType.BinaryInv);
            CvInvoke.Erode(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
            CvInvoke.Dilate(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);

            return thresh;
        }

        private Mat FilterPlate(Mat plate)
        {
            Mat thresh = new Mat();
            CvInvoke.Threshold(plate, thresh, 100, 255, ThresholdType.Binary);
            CvInvoke.Erode(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
            CvInvoke.Dilate(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);

            return thresh;
        }

        private List<UMat> FilterPlates(List<UMat> plates)
        {
            List<UMat> threshes = new List<UMat>();
            foreach (var plate in plates)
            {
                UMat thresh = FilterPlate(plate);
                threshes.Add(thresh.Clone());
            }
            return threshes;
        }

        //Mat filteredGray_global;
        private void Recognize(Image imageSource)
        {            
            string number = "";
            string number2 = "";
            using (Bitmap bitMap = new Bitmap(imageSource))
            using (Bitmap ExtendedBitMap = extendBitmap(bitMap, new Size(bitMap.Width, bitMap.Height + 20), new Point(0, 10), Brushes.White))
            using (Mat imageMat = BitMapToMat(ExtendedBitMap))//В нужный формат
            using (Mat gray = new Mat()) //Серое
            {
                CvInvoke.CvtColor(imageMat, gray, ColorConversion.Bgr2Gray);
                using (Mat grayResized = Resize(gray, new Size(600, 150))) //Изменен размер
                using (Mat filteredGray = FilterPlate(grayResized))//Контуры
                using (Mat canny = new Mat())//Контуры
                {
                    //filteredGray_global = filteredGray;//для отладки
                    CvInvoke.Canny(filteredGray, canny, 50, 100, 3, false);

                    using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())//Контуры                    
                    using (Mat hierarchy = new Mat())//Какая то фигня
                    {
                        CvInvoke.FindContours(canny, contours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                        //int[,] hierachy = CvInvoke.FindContourTree(canny, contours, ChainApproxMethod.ChainApproxSimple);
                        List<RotatedRect> boxes = getBoxesFromContours(contours, new Size(10, 10), new Size(80, 100)); //Которбки не меньше указанного размера
                        //List<UMat> boxes_sizeFiltered = getNormImagesInContours(boxes, grayResized); //для отладки
                        List<RotatedRect> boxes_smartFiltered = removeRepeatedBoxes(boxes);//Убираем повторения
                        List<RotatedRect> boxes_sort = sortBoxes(boxes_smartFiltered);//Сортируем в правильном порядке
                        List<UMat> plates = getNormImagesInContours(boxes_sort, filteredGray); //Картинки из контуров
                        List<UMat> filteredPlates = FilterPlates(plates); //Отфильтрованные   
                        List<UMat> resizedFilteredPlates = Resize(filteredPlates, new Size(240, 180)); //меняем размер 
                        List<Bitmap> segments = bitMapFromUmat(resizedFilteredPlates);
                        List<Bitmap> extendedContours = extendBitmaps(segments, 40, 40, new Point(20, 20), Brushes.Black);
                        number = getNumberFromContours(extendedContours);
                        //Mat testtest = FilterPlateMat(filteredGray);
                        //number2 = getNumberFromPlate(testtest);
                        //AddBitmapToLog(grayResized.Clone().Bitmap);
                        //AddBitmapToLog((Bitmap)filteredGray.Bitmap.Clone());
                        AddBitmapToLog(canny.Clone().Bitmap);                        
                        //AddUmatToLog(filteredPlates);
                        //AddUmatToLog(boxes_sizeFiltered);
                        AddBitmapsToLog(extendedContours);                      
                        boxes = null;
                        boxes_smartFiltered = null;
                        boxes_sort = null;
                        plates = null;
                        filteredPlates = null;
                        resizedFilteredPlates = null;

                    }
                }
            }

            number = number.Replace("\n", "");
            number = number.ToUpper();
            //numbers.Add("################");
            string str = checkNumber(number);
            if (str.Length==6)
            {
                numbers.Add(""+str);
            }
                

            //numbers.Add("$$ " + number2);        
        }

        string checkNumber(string number)
        {
            char rus_O = 'О';
            char eng_O = 'O';
            string rus_chars = "АВЕКМНОРСТУХ";
            string eng_chars = "ABEKMHOPCTYX";
            string pattern = @"["+ rus_chars + "0][0-9"+ rus_O + "][0-9"+ rus_O + "][0-9"+ rus_O + "]["+rus_chars+"0]["+rus_chars+"0]";
            MatchCollection match = Regex.Matches(number, pattern, RegexOptions.IgnoreCase);

            string pattern2 = @"["+ eng_chars + "0][0-9"+eng_O+"][0-9"+eng_O+"][0-9"+eng_O+"]["+ eng_chars + "0]["+ eng_chars + "0]";
            MatchCollection match2 = Regex.Matches(number, pattern2, RegexOptions.IgnoreCase);
            StringBuilder result = new StringBuilder();
            if (match.Count>0)
            {
                result.Append(match[0].Value);   
            }

            if (match2.Count > 0)
            {
                result.Append(match2[0].Value);

                for (int i = 0; i < eng_chars.Length; i++)
                {
                    result = result.Replace(eng_chars[i], rus_chars[i]);
                }
            }

            if (result.Length == 6)
            {
                if (result[0] == '0')
                    result[0] = rus_O;

                if (result[1] == rus_O)
                    result[1] = '0';

                if (result[2] == rus_O)
                    result[2] = '0';

                if (result[3] == rus_O)
                    result[3] = '0';

                if (result[4] == '0')
                    result[4] = rus_O;

                if (result[5] == '0')
                    result[5] = rus_O;
            }

            return result.ToString();
        }

        public List<string> getNumberCar(Bitmap image)
        {
            images_steps.Clear();
            List<Bitmap> numberPlate = findNumberPlate(image);
            if(numberPlate.Any())
                Recognize(numberPlate[0]);
            numberPlate = null;
            return numbers;
        }

        private Rectangle boundingRectangle(List<Point> points)
        {
            // Add checks here, if necessary, to make sure that points is not null,
            // and that it contains at least one (or perhaps two?) elements

            var minX = points.Min(p => p.X);
            var minY = points.Min(p => p.Y);
            var maxX = points.Max(p => p.X);
            var maxY = points.Max(p => p.Y);

            var height = maxY - minY;
            var width = maxX - minX;
            var needWidth = height * 6.5;
            //maxX += (int)(needWidth - width);

            return new Rectangle(new Point(minX, minY), new Size(maxX - minX, maxY - minY));
        }

        List<Bitmap> numberPlate;
        List<string> numbers;
        private List<Bitmap> findNumberPlate(Bitmap image)
        {
            numberPlate.Clear();
            numbers.Clear();
            string config_file = "openalpr.conf";
            string runtime_data_dir = "runtime_data";
            using (var alpr = new AlprNet("eu", config_file, runtime_data_dir))
            {
                if (!alpr.IsLoaded())
                {
                    throw(new Exception("Error initializing OpenALPR"));                    
                }

                alpr.DefaultRegion = "ru";                

                AlprResultsNet results = alpr.Recognize(image);
                
                foreach (var result in results.Plates)
                {
                    foreach(var topNPlates in result.TopNPlates)
                    {
                        string numb = checkNumber(topNPlates.Characters);
                        if (numb.Length == 6)
                            numbers.Add(numb);
                        List<Point> points = result.PlatePoints;
                        Rectangle rect = boundingRectangle(points);
                        points = null;
                        using (Bitmap cropped = cropImage(image, rect))
                            numberPlate.Add(new Bitmap(cropped));
                    }                                       
                }

                results = null;    
            }
            
            return numberPlate;
        }

        private Bitmap cropImage(Bitmap img, Rectangle cropArea)
        {
            try
            {
                return img.Clone(cropArea, img.PixelFormat);
            }
            catch (OutOfMemoryException ex)
            {
                return (Bitmap)img.Clone();
            }
        }

    }
}
