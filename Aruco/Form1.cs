﻿//----------------------------------------------------------------------------
//  Copyright (C) 2004-2016 by EMGU Corporation. All rights reserved.       
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Aruco;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading;

namespace Aruco
{
   public partial class Form1 : Form
   {
        List<Button> buttons;
        public delegate void SendData(int[] arr);
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;


        public Form1()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase("sp_db");
            int size = 35, otstup = 5;
            InitializeComponent();
            buttons = new List<Button>();
            for(int i = 0; i<markersX; i++)
            {
                for(int j = 0; j<markersY; j++)
                {
                    Button button = new Button();
                    button.Text = "A" + buttons.Count;
                    button.Size = new Size(size, size);
                    button.Location = new Point(j * (size + otstup), i * (size + otstup));
                    button.Name = "A" + +buttons.Count;
                    button.Enabled = false;
                    panel1.Controls.Add(button);
                    buttons.Add(button);
                }
            }

            FormSettings.Load();  
            UpdateMessage(String.Empty);
        }

        #region Инициализация камеры
        void StartCamera()
        {            
            ThreadStart threadStart = new ThreadStart(tryConnect);
            Thread thread = new Thread(threadStart);
            thread.Start();                       
        }

        void tryConnect()
        {
            string ip = FormSettings.IP_Address;
            string port = FormSettings.Port.ToString();
            string login = FormSettings.Login;
            string password = FormSettings.Password;
        //string connection_string = "rtsp://" + login + ":" + password + "@" + ip + "/StreamingSetting?version=1.0&action=getRTSPStream&ChannelID=1&ChannelName=Channel1";
            //string connection_string = "rtsp://" + login + ":" + password + "@" + ip + "/viewvideo.cs?action=get&ChannelID=1&ChannelName=Channel1";
            string connection_string = "rtsp://" + login + ":" + password + "@" + ip+ "/viewvideo.cs?action=getRTSPStream&ChannelID=1&ChannelName=Channel1";
            if (FormSettings.ThisIsNetCamera)
            {
                _capture = new VideoCapture(connection_string);//Сетевая камера
            }
            else
            {
                _capture = new VideoCapture();//USB камера
            }
            _capture.ImageGrabbed += ProcessFrame;
            StartCamera2();
        }
        #endregion

        private VideoCapture _capture = null;
        private bool _captureInProgress;
        private bool _useThisFrame = false;

        int markersX = 4;
        int markersY = 4;
        int markersLength = 80;
        int markersSeparation = 30;

        private Dictionary _dict;

        private Dictionary ArucoDictionary
        {
            get
            {
                if (_dict == null)
                    {
                        _dict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict4X4_100);                    
                    }
               
                return _dict;
            }         
        }

        private GridBoard _gridBoard;
        private GridBoard ArucoBoard
        {
            get
            {
            if (_gridBoard == null)
            {
                _gridBoard = new GridBoard(markersX, markersY, markersLength, markersSeparation, ArucoDictionary);
            }
            return _gridBoard;
            }
        }

        private void printArucoBoardButton_Click(object sender, EventArgs e)
        {
            Size imageSize = new Size();
            int margins = markersSeparation;
            imageSize.Width = markersX * (markersLength + markersSeparation) - markersSeparation + 2 * margins;
            imageSize.Height = markersY * (markersLength + markersSeparation) - markersSeparation + 2 * margins;
            int borderBits = 1;

            Mat boardImage = new Mat();
            ArucoBoard.Draw(imageSize, boardImage, margins, borderBits);
            bmIm = boardImage.Bitmap;
            PrintImage();
        }

        private void PrintImage()
        {
            PrintDocument pd = new PrintDocument();
            //pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            //pd.OriginAtMargins = false;
            //pd.DefaultPageSettings.Landscape = true;

            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);

            PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();

            printPreviewDialog1.Document = pd;
            //printPreviewDialog1.AutoScale = true;
            printPreviewDialog1.ShowDialog();         
        }

        Image bmIm;

        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            double cmToUnits = 100 / 2.54;
            e.Graphics.DrawImage(bmIm, 0, 0, (float)(15 * cmToUnits), (float)(15 * cmToUnits));
        }

        Mat _frame = new Mat();
        Mat _frameCopy = new Mat();

        Mat _cameraMatrix = new Mat();
        Mat _distCoeffs = new Mat();
        Mat rvecs = new Mat();
        Mat tvecs = new Mat();

        private VectorOfInt _allIds = new VectorOfInt();
        private VectorOfVectorOfPointF _allCorners = new VectorOfVectorOfPointF();
        private VectorOfInt _markerCounterPerFrame = new VectorOfInt();
        private Size _imageSize = Size.Empty;

        int errors = 0;
        void sendData(int[] arr)
        {            
            List<int> engaged = new List<int>(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

            for (int i = 0; i < arr.Length; i++)
            {
                engaged.Remove(arr[i]);
            }

            for (int i = 0; i < engaged.Count; i++)
            {
                int index = engaged[i];
                buttons[index].Enabled = false;
            }

            for (int i = 0; i < arr.Length; i++)
            {
                int index = arr[i];
                if (index < buttons.Count)
                {
                    buttons[index].Enabled = true;
                }
                else
                {
                    errors++;
                    messageLabel.Text = "ОШибок " + errors.ToString();
                }
            }

            
        }
        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero)
            {
            _capture.Retrieve(_frame, 0);
            
            //cameraImageBox.Image = _frame;

            using (VectorOfInt ids = new VectorOfInt())
            using (VectorOfVectorOfPointF corners = new VectorOfVectorOfPointF())
            using (VectorOfVectorOfPointF rejected = new VectorOfVectorOfPointF())
            {            
                DetectorParameters p = DetectorParameters.GetDefault();                    
                ArucoInvoke.DetectMarkers(_frame, ArucoDictionary, corners, ids, p, rejected);
                ArucoInvoke.RefineDetectedMarkers(_frame, ArucoBoard, corners, ids, rejected, null, null, 10, 3, true, null, p);
                _frame.CopyTo(_frameCopy);

                int[] arr = ids.ToArray();
                SendData sd = sendData;
                try
                {
                    this.Invoke(sd, arr);
                }
                catch(ObjectDisposedException ex)
                {

                }                

                if (ids.Size > 0)
                {                                      
                    //cameraButton.Text = "Calibrate camera";
                    this.Invoke((Action) delegate
                    {
                        useThisFrameButton.Enabled = true;
                    });
                    ArucoInvoke.DrawDetectedMarkers(_frameCopy, corners, ids, new MCvScalar(0, 255, 0));

                    if (!_cameraMatrix.IsEmpty && !_distCoeffs.IsEmpty)
                    {                    
                        ArucoInvoke.EstimatePoseSingleMarkers(corners, markersLength, _cameraMatrix, _distCoeffs, rvecs, tvecs);
                        for (int i = 0; i < ids.Size; i++)
                        {
                            using (Mat rvecMat = rvecs.Row(i))
                            using (Mat tvecMat = tvecs.Row(i))
                            using (VectorOfDouble rvec = new VectorOfDouble())
                            using (VectorOfDouble tvec = new VectorOfDouble())
                            {
                                double[] values = new double[3];
                                rvecMat.CopyTo(values);
                                rvec.Push(values);
                                tvecMat.CopyTo(values);
                                tvec.Push(values);                           
                                ArucoInvoke.DrawAxis(_frameCopy, _cameraMatrix, _distCoeffs, rvec, tvec, markersLength*0.5f);                           
                            }
                        }
                    }

                    if (_useThisFrame)
                    {
                        _allCorners.Push(corners);
                        _allIds.Push(ids);
                        _markerCounterPerFrame.Push(new int[] { corners.Size });
                        _imageSize = _frame.Size;
                        UpdateMessage(String.Format("Using {0} points", _markerCounterPerFrame.ToArray().Sum()));
                        _useThisFrame = false;
                    }
                }
                else
                {
                    this.Invoke((Action) delegate
                    {
                        useThisFrameButton.Enabled = false;
                    });

                    //cameraButton.Text = "Stop Capture";
                }
                cameraImageBox.Image = _frameCopy;
            }
            }
        }

        private void UpdateMessage(String message)
        {
            if (this.InvokeRequired)
            {
            this.Invoke((Action) delegate { UpdateMessage(message); });
            return;
            }

            messageLabel.Text = message;
        }

        private void cameraButton_Click(object sender, EventArgs e)
        {   
            if (_captureInProgress)
            {
                int totalPoints = _markerCounterPerFrame.ToArray().Sum();
                if (totalPoints > 0)
                {
                    double repError = ArucoInvoke.CalibrateCameraAruco(_allCorners, _allIds, _markerCounterPerFrame, ArucoBoard, _imageSize,
                        _cameraMatrix, _distCoeffs, null, null, CalibType.Default, new MCvTermCriteria(30, double.Epsilon));

                    UpdateMessage(String.Format("Camera calibration completed with reprojection error: {0}", repError));
                    _allCorners.Clear();
                    _allIds.Clear();
                    _markerCounterPerFrame.Clear();
                    _imageSize = Size.Empty;

                }
                //stop the capture
                timer1.Enabled = false;
                cameraButton.Text = "Start Capture";
                _capture.Pause();
                _captureInProgress = false;
            }
            else
            {                             
                StartCamera();
                _captureInProgress = true;
            }  
        }

        delegate void VoidFunction();
        void StartCamera2()
        {
            if (this.cameraButton.InvokeRequired)
            {
                VoidFunction del = new VoidFunction(StartCamera2);
                this.Invoke(del);
            }
            else
            {
                timer1.Enabled = true;
                //start the capture
                cameraButton.Text = "Stop Capture";
                _capture.Start();
            }
                
        }

        private void useThisFrameButton_Click(object sender, EventArgs e)
        {
            _useThisFrame = true;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            FormSettings FS = new FormSettings();
            FS.ShowDialog();
        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            for(int i=0;i<10; i++)
            {
                messageLabel.Text = "Ошибок " + errors.ToString();
                var collection = _database.GetCollection<BsonDocument>("parking");
                var filter = Builders<BsonDocument>.Filter.Eq("name", "A"+i);
                bool reserved = !buttons[i].Enabled;
                var update = Builders<BsonDocument>.Update.Set("occupied", reserved);
                try
                {
                    var result = await collection.UpdateOneAsync(filter, update);
                }
                catch(Exception ex)
                {
                    messageLabel.Text = "Не удалось обновить БД " + ex.Message;
                }
               
            }           
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //stop the capture
            timer1.Enabled = false;
            cameraButton.Text = "Start Capture";
            if(_capture != null)
                _capture.Pause();
        }
    }
}
